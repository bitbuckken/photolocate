-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           5.5.40-0ubuntu0.14.04.1 - (Ubuntu)
-- Serveur OS:                   debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de table photolocate. parties
CREATE TABLE IF NOT EXISTS `parties` (
  `partie_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id des partie cree',
  `partie_score` int(11) DEFAULT NULL COMMENT 'le score a la fin de la partie',
  `partie_joueur` varchar(50) DEFAULT NULL COMMENT 'le nom du joeur sur la partie',
  `partie_token` varchar(255) NOT NULL COMMENT 'le token associe a la partie',
  `partie_ville_id` int(11) NOT NULL COMMENT 'ville de la partie',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'mise à jours',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'création ',
  PRIMARY KEY (`partie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Export de données de la table photolocate.parties: ~4 rows (environ)
/*!40000 ALTER TABLE `parties` DISABLE KEYS */;
INSERT INTO `parties` (`partie_id`, `partie_score`, `partie_joueur`, `partie_token`, `partie_ville_id`, `updated_at`, `created_at`) VALUES
  (1, 0, 'boby', '', 1, NULL, NULL),
  (2, 1212, 'tt', '', 1, NULL, NULL),
  (3, NULL, NULL, '', 0, '2015-02-09 16:33:07', '2015-02-09 16:33:07'),
  (4, NULL, NULL, '', 0, '2015-02-09 16:40:01', '2015-02-09 16:40:01');
/*!40000 ALTER TABLE `parties` ENABLE KEYS */;


-- Export de la structure de table photolocate. partie_photo
CREATE TABLE IF NOT EXISTS `partie_photo` (
  `partie_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  KEY `foreign_key_liaison_partie_id` (`partie_id`),
  KEY `foreign_key_liaison_photo_id` (`photo_id`),
  CONSTRAINT `foreign_key_liaison_partie_id` FOREIGN KEY (`partie_id`) REFERENCES `parties` (`partie_id`),
  CONSTRAINT `foreign_key_liaison_photo_id` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table qui lie des photos a une partie.';

-- Export de données de la table photolocate.partie_photo: ~10 rows (environ)
/*!40000 ALTER TABLE `partie_photo` DISABLE KEYS */;
INSERT INTO `partie_photo` (`partie_id`, `photo_id`) VALUES
  (1, 2),
  (1, 3),
  (1, 8),
  (1, 11),
  (1, 12),
  (1, 15),
  (1, 1),
  (1, 18),
  (1, 21),
  (1, 22);
/*!40000 ALTER TABLE `partie_photo` ENABLE KEYS */;


-- Export de la structure de table photolocate. photos
CREATE TABLE IF NOT EXISTS `photos` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id des photos',
  `photo_url` varchar(255) NOT NULL DEFAULT '0' COMMENT 'url pour retrouver la photo sur serveur',
  `photo_longitude` double NOT NULL DEFAULT '0' COMMENT 'longitude de possitionnement de la photo',
  `photo_latitude` double NOT NULL DEFAULT '0' COMMENT 'latitude de possitionnement de la photo',
  `photo_ville_id` int(11) NOT NULL DEFAULT '0' COMMENT 'photo appartien a la ville',
  PRIMARY KEY (`photo_id`),
  KEY `foreign_key_photos_villes` (`photo_ville_id`),
  CONSTRAINT `foreign_key_photos_villes` FOREIGN KEY (`photo_ville_id`) REFERENCES `villes` (`ville_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COMMENT='table qui regroupes les photos pour le jeu\r\n\r\nlongitude latitude';

-- Export de données de la table photolocate.photos: ~10 rows (environ)
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` (`photo_id`, `photo_url`, `photo_longitude`, `photo_latitude`, `photo_ville_id`) VALUES
  (1, '/img/nancy_pepiniere.jpg', 6.185092, 48.698322, 1),
  (2, '/img/nancy_comforama.jpg', 6.204301, 48.679659, 1),
  (3, '/img/nancy_gare.jpg', 6.174405, 48.689983, 1),
  (8, '/img/nancy_iut_charlemagne.jpg', 6.161134, 48.682932, 1),
  (11, '/img/nancy_kinepolis.jpg', 6.196008, 48.691999, 1),
  (12, '/img/nancy_marcel_picot.jpg', 6.210408, 48.695456, 1),
  (15, '/img/nancy_parc_saint_marie.jpg', 6.170583, 48.680658, 1),
  (18, '/img/nancy_place_cariere.jpg', 6.181716, 48.695915, 1),
  (21, '/img/nancy_quais_sainte_catherine.jpg', 6.191293, 48.694834, 1),
  (22, '/img/nancy_stanislas.jpg', 6.183252, 48.693756, 1);
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;


-- Export de la structure de table photolocate. villes
CREATE TABLE IF NOT EXISTS `villes` (
  `ville_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la ville',
  `ville_libelle` varchar(50) NOT NULL DEFAULT '0' COMMENT 'nom de la ville',
  `ville_longitude` double NOT NULL DEFAULT '0' COMMENT 'longitude de la ville',
  `ville_latitude` double NOT NULL DEFAULT '0' COMMENT 'latitude de la ville',
  `ville_zoome` double NOT NULL DEFAULT '0' COMMENT 'zoome de la carte',
  PRIMARY KEY (`ville_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='zone de jeu\r\n\r\nlongitude latitude';

-- Export de données de la table photolocate.villes: ~1 rows (environ)
/*!40000 ALTER TABLE `villes` DISABLE KEYS */;
INSERT INTO `villes` (`ville_id`, `ville_libelle`, `ville_longitude`, `ville_latitude`, `ville_zoome`) VALUES
  (1, 'Nancy', 6.184417, 48.692054, 0);
/*!40000 ALTER TABLE `villes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;