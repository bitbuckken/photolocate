RENAME TABLE `parties_photos` TO `partie_photo`;
ALTER TABLE `parties`
	ADD COLUMN `partie_ville_id` INT NOT NULL COMMENT 'ville de la partie' AFTER `partie_token`;

ALTER TABLE `parties`
	ADD COLUMN `partie_statut` INT COMMENT 'statut de la partie';