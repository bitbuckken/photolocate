myApp = angular.module('photolocatApp')

myApp.controller 'homeController', ['$scope','$routeParams', 'scoresFactory', '$http', '$location', ($scope, $routeParams, scoresFactory, $http, $location)->




	###
		Item pour le scope et le binding
	###
	$scope.pseudo = ""
	$scope.messageSuccess = false
	$scope.jouer = ()->
		create_game_on_server()


	recuperer_ville = ()->
		data = $http.get "/play/games/ville"


	recuperer_ville().success((data)->
		$scope.villes = data
		
		)
	
	###
		function creation de partie quand on passe  le pseudo
	###
	create_game_on_server = ()->
		if($scope.ville_id==undefined)
			v = 0
		else
			v = $scope.ville_id
		
		$http {
			method: 'POST'
			url: '/play/games'
			headers: 'Content-Type': 'application/x-www-form-urlencoded'
			transformRequest: (obj) ->
				str = []
				for p of obj
					str.push encodeURIComponent(p) + '=' + encodeURIComponent(obj[p])
				str.join '&'
			data: {pseudo: $scope.pseudo,ville:v}
		}
		.success((data)->
			if(data.msgValidPseudo != undefined)
				$scope.msgValidPseudo = data.msgValidPseudo
			else
				
				set_locale_storage("photolocat_compteur", 0)
				set_locale_storage("photolocat_score", 0)

				set_locale_storage("photolocat_id", data.id)
				set_locale_storage("photolocat_token", data.token)
				set_locale_storage("photolocat_pseudo", $scope.pseudo)
				$location.path('/game')
		).error((error)->
			console.log error
		)


	###
		function pour mettre dans le locale storage id et le token de la partie
	###

	set_locale_storage = (libelle, obj)->
		window.localStorage.setItem( libelle , obj )

	getScore = ->
	scoresFactory.getScores(10).success((custs) ->
	  console.log custs
	  $scope.scores = custs
	  
	  
	).error (error) ->
	  console.log "error"
	  


	getScore()
]