 
 angular.module('photolocatApp').controller 'gameController', ['$routeParams','$scope','$http','photosFactory','$location',($routeParams,$scope,$http, photosFactory,$location) ->
    
    
    
    compteur = window.localStorage['photolocat_compteur']

    score = window.localStorage['photolocat_score'] 
    
    map = ""
    marker1 = ""
    marker2 = ""
    trait = ""
   
    
    
    if(compteur > 9)
      put_score_on_server(score,window.localStorage["photolocat_id"],window.localStorage["photolocat_token"])
    
    set_locale_storage = (libelle, obj)->
      window.localStorage.setItem( libelle , obj )
 
    createMap = (lat,lng)->
        
        map = L.map(document.getElementById("mapleaflet"), {
          center:[lat, lng],
          zoom:13,
          minZoom:11,
          maxZoom:16
        });
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);  
        


    

    $scope.changeEtape = () ->

          
          map.removeLayer(marker1)
          map.removeLayer(marker2)
          map.removeLayer(trait)
          marker1= ""
          
          
          
          

          
        
        
          $scope.image = $scope.photos[compteur]['photo_url']
          window.localStorage['photolocat_temps'] = new Date().getTime();
          console.log window.localStorage['photolocat_temps']
         
         

    getConfig = ->
       
      photosFactory.getConfig().success((custs) ->
        custs
          

          
      ).error (error) ->
        console.log error

    getPhoto = ->
       
      photosFactory.getPhotos().success((custs) ->
        $scope.image = custs[compteur]['photo_url']
        window.localStorage['photolocat_temps'] = new Date().getTime();
        console.log window.localStorage['photolocat_temps']
        $scope.photos = custs
               

      ).error (error) ->
        console.log error

    getConfig().then((data)->
        lat = data['data'][0]['ville_latitude']
        long = data['data'][0]['ville_longitude']
    

        createMap(lat,long)

        map.on("click",(e)->

          if(marker1=="")
            marker1 =  L.marker([e.latlng.lat,e.latlng.lng],{icon:  L.icon({iconUrl: '/assets/images/marker_icon_blue.png',  iconAnchor:[12.5, 41],})}).addTo(map)

            c = window.localStorage['photolocat_compteur']
          
          

          
          
            marker2 =  L.marker([$scope.photos[c]["photo_latitude"],$scope.photos[c]["photo_longitude"]],{icon:  L.icon({iconUrl: '/assets/images/marker_icon_green.png',  iconAnchor:[12.5, 41],})}).addTo(map)
            
            p1 = new L.LatLng(e.latlng.lat,e.latlng.lng)
            p2 = new L.LatLng(parseFloat($scope.photos[c]["photo_latitude"]),parseFloat($scope.photos[c]["photo_longitude"]))
         
            

            trait = new L.Polyline( [p1,p2], {
             color: 'red',
             weight: 5,
             opacity: 0.5
             smoothFactor: 1

            });

            map.addLayer(trait);
            
            if localStorage.getItem('photolocat_compteur') > 8
              finPartie(e.latlng.lat,e.latlng.lng)
            else affichageScore(e.latlng.lat,e.latlng.lng)
          
            console.log "ok"
            new_value =  parseInt(localStorage.getItem('photolocat_compteur')) + 1

          
         
          
            window.localStorage['photolocat_compteur'] = new_value
            compteur = new_value
            $scope.compteurProgress = compteur*10
            $scope.$apply()
          )
        
        
      )
    ###
    function put de score
    ###
    put_score_on_server = (sc,id,token)->
      $http {
        method: 'PUT'
        url: '/play/games/'+id+"?token="+token
        headers: 'Content-Type': 'application/x-www-form-urlencoded'
        transformRequest: (obj) ->
          str = []
          for p of obj
           str.push encodeURIComponent(p) + '=' + encodeURIComponent(obj[p])
          str.join '&'
        data: {s: score}
      }
      .success((data)->
        $location.path('/fin')
      ).error((error)->
        console.log error
      )

    $scope.endGame = ()->
      put_score_on_server(score,window.localStorage["photolocat_id"],window.localStorage["photolocat_token"])
      

    finPartie = (lat,long2)->
      

      sc = calculScore($scope.photos[compteur]["photo_latitude"],$scope.photos[compteur]["photo_longitude"],lat,long2)
      score = parseInt(score) + parseInt(sc)
      console.log score
      window.localStorage["photolocat_score"]=parseInt(localStorage.getItem('photolocat_score')) + parseInt(sc)

      if(parseInt(score)!=parseInt(window.localStorage["photolocat_score"]))
        alert "tentative de triche"
        
      
      $scope.btnNext = false
      $scope.$apply()
      $scope.btnFin = "Suivant"
      $scope.$apply()

      

    affichageScore = (lat,long2)->
      
      c = window.localStorage['photolocat_compteur']
      sc = calculScore($scope.photos[c]["photo_latitude"],$scope.photos[c]["photo_longitude"],lat,long2)
      document.getElementById("scorePlus").style.visibility = "visible"
      $scope.score = sc
      setTimeout("document.getElementById(\"scorePlus\").style.visibility = \"hidden\"", 2000)
      window.localStorage["photolocat_score"]= parseInt(localStorage.getItem('photolocat_score')) + sc
      sTotal = window.localStorage["photolocat_score"]
      $scope.$apply()
      

      $scope.scoreTotal = sTotal
      score = sTotal
      $scope.btnNext = "Suivant"
      console.log window.localStorage["photolocat_score"]
      
      $scope.$apply()

    
      
     
      
      

     


    
    toRad = (Value)->
      Value * Math.PI / 180;

    calculScore = (lat1, lon1, lat2, lon2)->
      
      D1 = 70
      D2 = 140
      D3 = 210

      R = 6371
      dLat = toRad(lat2-lat1)
      dLon = toRad(lon2-lon1) 
      a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        
      c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      d = R * c*1000;
      
      

      if d < 70 
        scoreT = 100
      else if d < 140 
        scoreT = 50
      else if d < 210 
        scoreT = 30
      else scoreT = 10


      temps =  ~~( (new Date().getTime() - parseInt(window.localStorage['photolocat_temps']))/1000)
  
      if temps < 10
        scoreT = scoreT*(11-temps)

      scoreT


    getPhoto()
     
  ]
