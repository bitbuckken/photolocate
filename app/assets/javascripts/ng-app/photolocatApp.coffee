app = angular.module( 'photolocatApp', ['ngRoute','ngResource'])

app.config(['$routeProvider',($routeProvider)->
		$routeProvider.when('/',{
			templateUrl: "/angular/accueil",
			controller: "homeController"	
		})

		$routeProvider.when('/game',{
			templateUrl: "/angular/game",
			controller: "gameController"	
		})

		$routeProvider.when('/scores',{
			templateUrl: "/angular/scores",
			controller: "scoreController"
		})

		$routeProvider.when('/fin',{
			templateUrl: "/angular/fin",
			controller: "finController"
		})
	]
);
