<?php
namespace MFWK\controllers;

use Respect\Validation\Validator as v;

class HomeController {

	function checkPseudo() {
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);

		$post = $app->request->post();


		$validPseudo = v::notEmpty()->alnum()->length(3,25)->validate($post["pseudoGame"]);
		if (!$validPseudo) {
			$msgvalidPseudo ="Le titre est trop court ou trop long.";
			$error['msgvalidPseudo'] = $msgvalidPseudo;
			$view->assign("error", $error);
		}

		$view->render('accueil/accueil.tpl.php');
	}

}