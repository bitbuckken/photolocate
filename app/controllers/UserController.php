<?php
namespace MFWK\controllers;

use MFWK\models\User;
use Respect\Validation\Validator as v;


class UserController {


	function checkConnexion() {
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);

		$post = $app->request->post();

		$v = User::validateUserConnexion($post);

		if (!empty($v["msgNoValidConnexion"])) {
			$view->assign("errorConnexion", $v);
			$view->render('user/connexion.tpl.php');
		}
		else {
			$_SESSION["user"]["id"] = $v["user"]->user_id;
			$_SESSION["user"]["pseudo"] = $v["user"]->user_pseudo;
			$_SESSION["user"]["statut"] = $v["user"]->user_status;

			$app->redirect("/");
		}

		
	}

	function inscriptionUser() {
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);

		$post = $app->request->post();

		$v = User::validateUserInscription($post);

		if (!empty($v["erreur"])) {
			$view->assign("errorInscription", $v);
			$view->render('user/inscription.tpl.php');
		}
		else {
			$success = "Vous êtes inscris sur PhotoLocate, vous pouvez maintenant vous connecter";
			$view->assign("successInscription", $success);

			$user = new User();

			$user->user_pseudo = $v["datas"]["pseudo"];
			$user->user_email = $v["datas"]["email"];
			$user->user_password = password_hash($v["datas"]["password"], PASSWORD_DEFAULT);
			$user->save();
			$view->render('user/connexion.tpl.php');
		}
		

		
	}
	

}