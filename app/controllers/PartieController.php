<?php

namespace MFWK\controllers;

use MFWK\models\User;
use MFWK\models\Partie;
use MFWK\models\Photo;
use MFWK\models\Ville;
use Respect\Validation\Validator as v;

class PartieController {

	function toJson(){
		$app = \Slim\Slim::getInstance();
		$app->contentType('application/json');//pour dire qu'on ne renvoie que du Json
		$view = \MFWK\lib\View::getInstance();
		$view->disable();
	}

	function getPhoto($id,$token_url){		
		$game = Partie::find($id);

		$token_partie = $game->partie_token;
		
		

		if($token_partie==$token_url){

			if(is_object($game))
				$game = $game->photos->toJson();
			else 
				$game="{'error':'Une erreur est survenue !}";
		}
		else	
			$game="{'error':'Une erreur est survenue !}";

		
		
		$app = \Slim\Slim::getInstance();
		$app->contentType('application/json');//pour dire qu'on ne renvoie que du Json
		$view = \MFWK\lib\View::getInstance();
		$view->disable();
		

		echo $game;

	}
	
	public function index(){
		echo Partie::find(1)->toJson();
	}
	
	public function create_partie($pseudo,$ville){
		$retour = array();
		

		$validPseudo = v::notEmpty()->alnum()->length(3,25)->validate($pseudo);

		if (!$validPseudo) {
			$retour['msgValidPseudo'] = "Le pseudo est trop court ou trop long. Il ne doit contenir que des lettres et des chiffres"; 
		}
		else{



			// on crée une nouvelle partie
			$game = new Partie();

			// on donne a la partie le joueur et un score
			$game->partie_joueur = $pseudo;
			$game->partie_score = 0;
			
			//on sauvegarde la partie
			$game->save();
			$game = Partie::find($game->partie_id);
			
			//on cree le token
			$game->partie_token = $this->create_token($game->partie_id, $pseudo, $game->created_at);

			//on lui assigne une ville
			if(isset($villle)){
				if($ville!=0)
					$game->partie_ville_id = Ville::find($ville)->ville_id;
				else
					$game->partie_ville_id = Ville::orderByRaw("RAND()")->first()->ville_id;
			}
			else{
				$game->partie_ville_id = Ville::orderByRaw("RAND()")->first()->ville_id;
			}
			
			$v = $game->partie_ville_id ;
			
			// on met a jour la partie
			$game->save();	
			
			//on selectionne 10 photo 
			$photos = (array) current(Photo::where('photo_ville_id', '=' , $v)->orderByRaw("RAND()")->limit(10)->get());
			$game->photos()->saveMany($photos);
			$retour['id'] = $game->partie_id;
			$retour['token'] = $game->partie_token;
		
		}
		// on cree l obejt de retour
		
		
		

		$this->toJson();
		
		echo json_encode($retour);
	}
	
	private function create_token($partie_id,$pseudo, $created_ad) {
		//token = hash( 'sha256', partie_id.speudo.creat_at)
		return hash( 'sha256', $partie_id.$pseudo.$created_ad);

	}

	function getParties($id, $token){

		$game = Partie::where('parties.partie_id','=',$id)
                ->leftJoin('villes', 'parties.partie_ville_id', '=', 'villes.ville_id')
                ->get();
        echo $game->toJson();

		$app = \Slim\Slim::getInstance();
		$app->contentType('application/json');//pour dire qu'on ne renvoie que du Json
		$view = \MFWK\lib\View::getInstance();
		$view->disable();	
	}



	function finish($id, $score){
		//mise à jour de la partie, le score de l'utilisateur est enregistré, la partie est marquée comme achevée
		$game = Partie::find($id);
		$app = \Slim\Slim::getInstance();

		

		if(!is_object($game)) {
		}
		else {
			
			if($game->partie_statut != '2')
			{	
				

				
				$token = $_GET['token'];
				if ($token == $game->partie_token) {
					
					$game->partie_statut = '2';
					$game->partie_score = $score;
					$game->save();
				}
			}	
		}

		$this->toJson();
		echo "{}";
	}

	function getScores($nombre){
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);
		if(isset($nombre)) 
			$games = Partie::where('partie_statut', '=' , '2')->orderBy('partie_score', 'desc')->limit($nombre)->get();
		else 
			$games = Partie::where('partie_statut', '=' , '2')->orderBy('partie_score', 'desc')->get();
		echo $games;
		$app->contentType('application/json');//pour dire qu'on ne renvoie que du Json
		$view = \MFWK\lib\View::getInstance();
		$view->disable();	
	}

	function getPosition($idPartie){
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);
		
		$game = Partie::where('partie_id', '=' , $idPartie)->select("partie_score")->get();
		
		$gamejson = $game->toJson();
		
		$gamejson = explode(":", $gamejson);
		
		$gamejson = str_replace("}]","", $gamejson[1]);
		$gamejson = str_replace('"', '', $gamejson);
		
		//Compter les parties qui ont un score > au score de $game
		$position = Partie::where('partie_statut', '=' , '2')->where('partie_score', '>=' , $gamejson)->count();

		echo $position;
		$app->contentType('application/json');//pour dire qu'on ne renvoie que du Json
		$view = \MFWK\lib\View::getInstance();
		$view->disable();
		
		
	}

	function ajoutPhotosAdmin(){
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);
		
		//A faire : si l'utilisateur est admin et connecté :



		//récup des villes dans la base
		$villes = Ville::all();
		$view->assign('villes', $villes);

		$view->render('admin/ajout_photo.tpl.php');
	}

	function traitementPhoto(){
		$app = \Slim\Slim::getInstance();
		$view = \MFWK\lib\View::getInstance($app);
		
		$destination = 'img/';
		//test du formulaire d'ajout de photo
		$data = $app->request->post();
		$photo = $_FILES;
		$villeID = $data['ville'];
		$lieu = $data['nomLieu'];
		$lat = $data['latitude'];
		$long = $data['longitude'];

		
		$fichier = basename($_FILES['photo']['name']);
		$tailleCourante = filesize($_FILES['photo']['tmp_name']);
		$tailleMaxi = 10048576;
		$extensionsAutorisees = array('.png', '.gif', '.jpg', '.jpeg');
		$extensionCourante = strrchr($_FILES['photo']['name'], '.'); 

		//tests sur la photo
		if($photo != null) {
			if(!in_array($extensionCourante, $extensionsAutorisees))
			{
			    $erreurPhoto = 'Types de fichiers autorisés : png, gif, jpg, jpeg.';
			}
			if($tailleCourante>$tailleMaxi)
			{
			    $erreurPhoto = 'Le fichier est trop gros.';
			}
			if(isset($erreurPhoto)) {
				//var_dump("tutu"); die();
				$villes = Ville::all();
				$view->assign('villes', $villes);
				$view->assign('lieu', $lieu);
				$view->assign('latd', $lat);
				$view->assign('lng', $long);
				$view->assign('ErreurPhoto', $erreurPhoto);
				$view->render('admin/ajout_photo.tpl.php');
			}
			else { //Si pas d'erreur de photo, on teste les autres champs
				//Si un des champs n'est pas rempli
				if($villeID == null || $lieu == null || $lat == null || $long == null) {
					$error = "Veuillez remplir tous les champs";
					$villes = Ville::all();
					$view->assign('villes', $villes);
					$view->assign('ErrorMess', $error);
					$view->assign('lieu', $lieu);
					$view->assign('latd', $lat);
					$view->assign('lng', $long);
					$view->render('admin/ajout_photo.tpl.php');
				}
				else {
				    $ville = Ville::find($villeID);
				    $nomVille = $ville->ville_libelle;
					$dest = $destination.strtolower($nomVille)."_".strtolower($lieu).strtolower($extensionCourante); 
					if(move_uploaded_file($_FILES['photo']['tmp_name'], "../public/".$dest))
				    {
				    	
				    	$path= getcwd()."/".$dest;
				    	

				        $result = "Upload effectué avec succès !";
				     	chmod($path, 0777);  
				     	
				        $photoDB = new Photo;
						$photoDB->photo_url = $dest;
						$photoDB->photo_longitude = $long;
						$photoDB->photo_latitude = $lat;
						$photoDB->photo_ville_id = $villeID;
						$photoDB->save();
				    }
				    else
				    {
				        $result = "Echec de l\'upload !";
				    }
					$villes = Ville::all();
					$view->assign('villes', $villes);
					$view->assign('result', $result);
					$view->render('admin/ajout_photo.tpl.php');
				}
			}
		}


	}
}