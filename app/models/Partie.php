<?php

namespace MFWK\models;
use Respect\Validation\Validator as v;

class Partie extends \Illuminate\Database\Eloquent\Model  {

protected $table = 'parties';
	protected $primaryKey = 'partie_id';
	public $timestamps=true;
	
	 protected $fillabel = ['photo'];

 	
	public function photos(){

		return $this->belongsToMany('\MFWK\models\Photo');
	}

}
