<?php
 
namespace MFWK\models;
use Respect\Validation\Validator as v;

class Photo extends \Illuminate\Database\Eloquent\Model  {

protected $table = 'photos';
	protected $primaryKey = 'photo_id';
	public $timestamps=false;

	 	
	public function parties(){

		return $this->belongsToMany('\MFWK\models\Partie');
	}

}