<?php
namespace MFWK\models;

use MFWK\models\User;
use Respect\Validation\Validator as v;

class User extends \Illuminate\Database\Eloquent\Model  {

	protected $table = 'users';
	protected $primaryKey = 'user_id';
	public $timestamps=true;

	public static function validateUserConnexion($post) {

		$pseudo = $post["pseudo"];
		$password = $post["password"];

		$users = User::all();

		foreach ($users as $user) {
			if ($pseudo == $user->user_pseudo && password_verify($password, $user->user_password)) {
				$data["msgValidConnexion"] = 'Vous êtes maintenant connecté !';
				$userV = $user;
			}
		}

		if (empty($data["msgValidConnexion"])) 
			$data["msgNoValidConnexion"] = 'Le pseudo ou le mot de passe est incorrect';
		if (!empty($userV))
			$data["user"] = $userV;

		return $data;
	}

	public static function validateUserInscription($post) {

		$pseudo = $post["pseudo"];
		$email = $post["email"];
		$password = $post["password"];

		$error = null;

		$validPseudo = v::notEmpty()->alnum()->length(3,25)->validate($pseudo);
		if (!$validPseudo) {
			$msgvalidPseudo ="Le pseudo est trop court ou trop long.";
			$error['msgvalidPseudo'] = $msgvalidPseudo;
		}
		$user = User::where('user_pseudo', $pseudo)->first();	
		if (isset($user))
			$error['msgvalidPseudo'] = "Le pseudo est déjà utilisé";

		$validMail = v::notEmpty()->email()->validate($email);
		if (!$validMail) {
			$msgvalidMail ="L'adresse email n'est pas valide";
			$error['msgvalidMail'] = $msgvalidMail;
		}

		$validPassword = v::notEmpty()->alnum()->length(8,15)->validate($password);
		if (!$validPassword) {
			$msgvalidPassword ="Le mot de passe doit contenir entre 8 et 15 caractères";
			$error['msgvalidPassword'] = $msgvalidPassword;
		}

		$post["pseudo"] = htmlentities($pseudo, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$post["email"] = htmlentities($email, ENT_QUOTES | ENT_IGNORE, "UTF-8");


		$data["datas"] = $post;
		$data["erreur"] = $error; 

		return $data;
	}


}