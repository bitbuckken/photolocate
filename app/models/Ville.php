<?php
 
namespace MFWK\models;
use Respect\Validation\Validator as v;

class Ville extends \Illuminate\Database\Eloquent\Model  {

	protected $table = 'villes';
	protected $primaryKey = 'ville_id';
	public $timestamps=false;

	 	
	public function villes(){

		return $this->belongsToMany('\MFWK\models\Partie');
	}

}