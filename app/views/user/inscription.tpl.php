<?php 
  $error = $this->get("errorInscription");
  $success = $this->get("success"); 
?>

<div class="center">
  <form class="form-horizontal" action="/inscription" method="post">
    <fieldset>
      <legend>Inscription</legend>
      </br>
      <?php 
        if(!empty($success)) 
          echo '<div class="alert alert-success alertmsg" role="alert">'.$success.'</div>';
      ?>
      <div <?php echo 'class="formCust form-group '; echo (!empty($error["erreur"]["msgvalidPseudo"])) ? 'has-error"' : '"'; ?>>
        <label class="col-md-4 control-label" for="pseudo">Pseudo</label>
        <div class="col-md-4">
          <input id="pseudo" name="pseudo" placeholder="John" class="form-control input-md" type="text" value=<?php echo '"'; echo (!empty($error["datas"]["pseudo"])) ? htmlentities($error["datas"]["pseudo"], ENT_QUOTES | ENT_IGNORE, "UTF-8").'"' : '"'; ?>>
        </div> 
        <?php if(!empty($error["erreur"]["msgvalidPseudo"])) echo '<div class="alert alert-danger alertmsg" role="alert">'.$error["erreur"]["msgvalidPseudo"].'</div>'?>
      </div>
      <div <?php echo 'class="formCust form-group '; echo (!empty($error["erreur"]["msgvalidMail"])) ? 'has-error"' : '"'; ?>>
        <label class="col-md-4 control-label" for="email">Email</label>  
        <div class="col-md-4">
          <input id="email" name="email" placeholder="Email" class="form-control input-md" type="text" value=<?php echo '"'; echo (!empty($error["datas"]["email"])) ? htmlentities($error["datas"]["email"], ENT_QUOTES | ENT_IGNORE, "UTF-8").'"' : '"'; ?>>
        </div>
        <?php if(!empty($error["erreur"]["msgvalidMail"])) echo '<div class="alert alert-danger alertmsg" role="alert">'.$error["erreur"]["msgvalidMail"].'</div>'?>
      </div>
      <div <?php echo 'class="formCust form-group '; echo (!empty($error["erreur"]["msgvalidPassword"])) ? 'has-error"' : '"'; ?>>
        <label class="col-md-4 control-label" for="password">Mot de passe</label>  
        <div class="col-md-4">
          <input id="password" type="password" name="password" placeholder="Votre mot de passe" class="form-control input-md" >
        </div>
        <?php if(!empty($error["erreur"]["msgvalidPassword"])) echo '<div id="alertmsgPwd" class="alert alert-danger alertmsg" role="alert">'.$error["erreur"]["msgvalidPassword"].'</div>'?>          
      </div>
      <div class="formCust form-group">
        <label class="col-md-4 control-label" for="formInscription"></label>
        <div class="col-md-4">
          <button id="formInscription" class="btn btn-primary">S'inscrire</button>
          <input type="hidden" name="csrf_token" value="<?php echo $_SESSION["csrf_token"]; ?>">
        </div>
      </div>
    </fieldset>
  </form>
</div>

