<?php 
  $error = $this->get("errorConnexion");
  $success = $this->get("successInscription"); 
?>

<div class="center">
  <form class="form-horizontal" action="/connexion" method="post">
    <fieldset>
      <legend>Connexion</legend>
      </br>
      <?php 
        if(!empty($success)) 
          echo '<div class="alert alert-success alertmsg" role="alert">'.$success.'</div>';
        if (!empty($error))
          echo '<div class="alert alert-danger alertmsg" role="alert">'.$error["msgNoValidConnexion"].'</div>';
      ?> 
      <div <?php echo 'class="formCust form-group '; echo (!empty($error["msgNoValidConnexion"])) ? 'has-error"' : '"'; ?>>
        <label class="col-md-4 control-label" for="pseudo">Pseudo</label>
        <div class="col-md-4">
          <input id="pseudo" name="pseudo" placeholder="John" class="form-control input-md" type="text">
        </div> 
      </div>
      <div <?php echo 'class="formCust form-group '; echo (!empty($error["msgNoValidConnexion"])) ? 'has-error"' : '"'; ?>>
        <label class="col-md-4 control-label" for="password">Mot de passe</label>  
        <div class="col-md-4">
          <input id="password" type="password" name="password" placeholder="Votre mot de passe" class="form-control input-md" >
        </div>
      </div>
      <div class="formCust form-group">
        <label class="col-md-4 control-label" for="formInscription"></label>
        <div class="col-md-4">
          <button id="formInscription" class="btn btn-primary">Connexion</button>
          <input type="hidden" name="csrf_token" value="<?php echo $_SESSION["csrf_token"]; ?>">
        </div>
      </div>
    </fieldset>
  </form>
</div>

