<H1>Tableau des meilleurs scores.</H1>

<?php
$games = $this->tpl['vars']['game'];
$i = 1;
echo '<table class="table table-hover"';
echo '<tr><th>Classement</th><th>Pseudo</th><th>Score</th></tr>';
foreach ($games as $g) {
	echo '<tr>';
	echo '<td>'.$i.'</td><td>'.$g->partie_joueur.'</td><td>'.$g->partie_score.'</td>';	
	echo '</tr>';
	++$i;
}
echo '</table>';