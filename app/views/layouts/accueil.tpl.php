<!DOCTYPE html>
<html>
	<head>
		<title>Photolocate</title>
		<?= $this->stylesheet_tag('/assets/stylesheets/styles.css'); ?>
		<?= $this->stylesheet_tag('/assets/stylesheets/leaflet.css'); ?>
		<?= $this->stylesheet_tag('/assets/stylesheets/angular.css'); ?>

		<?= $this->javascript_tag('/assets/javascripts/lib/angular/angular.min.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/lib/angular/angular-route.min.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/lib/angular/angular-sanitize.min.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/lib/angular/angular-resource.min.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/dist/leaflet.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/dist/ng-app/photolocatApp.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/dist/ng-app/controllers/game_controller.js'); ?>
		<?= $this->javascript_tag('/assets/javascripts/dist/ng-app/controllers/home_controller.js'); ?>
		<?= $this->javascript_tag('/assets/javascripts/dist/ng-app/controllers/score_controller.js'); ?>
		<?= $this->javascript_tag('/assets/javascripts/dist/ng-app/controllers/fin_controller.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/dist/ng-app/factory/photos_factory.js'); ?>
        <?= $this->javascript_tag('/assets/javascripts/dist/ng-app/factory/scores_factory.js'); ?>
  
        

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="Photolocate">
  	</head>


	<body>

		<nav class="navbar navbar-default navbar-inverse navbar-static-top">
		    <div class="navbar-header">
		    	<a class="navbar-brand" href="/">
		        	<img alt="Logo du site" src="assets/images/logo.png">
		      	</a>
		      	<a class="navbar-brand" href="/">
		        	<h4>PhotoLocate</h4>
		      	</a>
		    </div>
		    <?php 
			    if (!empty($_SESSION["user"])) {
			    	echo '<p class="navbar-text navbar-right connecnav"> Connecté en tant que : <span id="pseudoConnec">
			    		'.$_SESSION["user"]["pseudo"].'</span></p>';
			    }
			?>
    
		</nav>

		<div class="container">
			<div class="main">
				<div ng-app="photolocatApp" ng-view></div>
			</div>
		</div> 
	</body>
</html>
