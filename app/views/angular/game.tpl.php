<img class="imgVille" ng-src="{{image}}" ng-show="image"  />
<div id="mapleaflet" class="mapWidth"></div>

<div id="scorePlus" class="alert alert-success" style="position:absolute;top:450px" ng-show="score">+ {{score}} pts</div>
<div class="alert alert-success mapWidth scoreSuivant" ng-show="score">score total : {{scoreTotal}} pts <div ng-show="btnNext" ng-click="changeEtape()" class=" btn btn-success btnNext" style="float:right">{{btnNext}}</div><div ng-show="btnFin" ng-click="endGame()" class="btn btn-info btnFin" style="float:right">{{btnFin}}</div></div>
<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{compteurProgress}}" aria-valuemin="0" aria-valuemax="100" style="width:{{compteurProgress}}%">
  </div>
</div>