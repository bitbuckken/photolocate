<H1>Tableau des meilleurs scores.</H1>

<a href="/Play#/"><button type="button" class="btn btn-primary">Accueil</button></a>
<table class="table table-hover">
	<tr><th>classement</th><th>Pseudo</th><th>Score</th></tr>
    <tr ng-repeat="score in scores track by $index">
        <td>
            {{$index+1}}
        </td>
        <td>
            {{score.partie_joueur}}
        </td>
        <td>
            {{score.partie_score}}
        </td>
    </tr>
</table>