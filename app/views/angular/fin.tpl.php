<div class="jumbotron center"> 
	<h1>Partie teminée !</h1>
	<p>
		Félicition {{pseudoJoueur}}, vous avez fait un score de {{scoreFinal}} points, ce qui vous place en numéro {{position}}.
	</p>
	<p>
		<a href="/Play#/scores"><button type="button" class="btn btn-primary">Tableau des scores</button></a> <a href="/Play#/"><button type="button" class="btn btn-primary">Rejouer</button></a>
	</p>
</div>