
<div class="page-header title">
	<h1 class="center">Règles du jeu</h1>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
	    <h3 class="panel-title">PhotoloQuoi ??</h3>
	</div>
	<div class="panel-body">

		PhotoLocate est un jeu simple dans lequel vous allez voir la photo d'un lieu que vous devrez placer sur la map. Vous devrez être précis et rapide pour être le meilleur !

		<p>Le principe est simple : une photo vous est proposée, vous devez indiquer la position de ce qu'elle 
			représente sur une carte d'une ville.</p>
		<ul>
			<li>Une partie consiste en une séquence de 10 photos à placer sur la carte de la ville</li>
			<li>Chaque réponse permet de gagner un certain nombre de points, en fonction de la
				précision du placement et de la rapidité pour répondre </li>
 			<li>L'objectif pour une partie est d'obtenir le maximum de points </li>
 			<li>La partie est terminée lorsque les 10 photos ont été positionnées </li>
 		</ul>
	</div>
</div>

<form class="form-inline center" role="form" ng-submit="jouer()">
	<div>

		<select ng-model="ville_id" class="form-control" >
			<option value="">Ville Aleatoire</option>
			 <option ng-repeat="ville in villes" value="{{ville.ville_id}}">{{ville.ville_libelle}}</option>
		</select>
    	<input type="text" ng-model="pseudo" name="pseudoGame" class="form-control" placeholder="Pseudo" >
    	<button type="submit" class="btn btn-primary btnPlay">Jouer</button> 
	</div>
</form>	
<div ng-show="msgValidPseudo" class="alert alert-danger alertmsg" role="alert">{{msgValidPseudo}}</div>
<div ng-show="messageSuccess" class="alert alert-success alertmsg" role="alert">{{messageSuccess}}</div>
<div class="form-inline center"><h2>Scores :</h2>
	<table class="table table-bordered">
	<tr><th>classement</th><th>Pseudo</th><th>Score</th></tr>
    <tr ng-repeat="score in scores | limitTo:5">
        <td>
            {{$index+1}}
        </td>
        <td>
            {{score.partie_joueur}}
        </td>
        <td>
            {{score.partie_score}}
        </td>
    </tr>
</table>
	<a href="/Play#/scores"><button type="button" class="btn btn-primary">Tableau complet</button></a>
</div>
