<div class="jumbotron center">
	<h1>Bienvenue sur PhotoLocate</h1>
	<p>Un jeu amusant où tu dois positionner une photo sur la carte de ta ville sans te tromper et 
		plus vite que les autres !
	</p>
	<p><a class="btn btn-primary btn-lg" href="/Play" role="button">Accéder au jeu</a></p>
</div>
