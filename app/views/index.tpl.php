<!DOCTYPE html>
<html>
	<head>
		<title>Photolocate</title>
		<?= $this->stylesheet_tag('/assets/stylesheets/styles.css'); ?>

		<?= $this->javascript_tag('/assets/javascripts/lib/bootstrap.min.js'); ?>
		<?= $this->javascript_tag('/assets/javascripts/lib/jquery-1.11.1.min.js'); ?>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="Photolocate">
  	</head>

	<body>

		<nav class="navbar navbar-default navbar-inverse navbar-static-top">
		    <div class="navbar-header">
		    	<span class="sr-only">Toggle navigation</span>
		    	<a class="navbar-brand" href="/">
		        	<img alt="Logo du site" src="assets/images/logo.png">
		      	</a>
		      	<a class="navbar-brand" href="/">
		        	<h4>PhotoLocate</h4>
		      	</a>
		      	<a class="btn btn-primary navbar-btn connecnav" href="/Play" role="button">Accéder au jeu</a>
		    </div>

		    <?php 
		    	if (!empty($_SESSION["user"])) {
		    		echo '<a href="/deconnexion"><button type="button" class="btn btn-primary navbar-btn navbar-right connecnav">Déconnexion</button></a>';
		    	}
		    	else {
		    		echo '
		    			<a href="/inscription"><button type="button" class="btn btn-primary navbar-btn navbar-right connecnav">Inscription</button></a>
			    		<a href="/connexion"><button type="button" class="btn btn-primary navbar-btn navbar-right connecnav">
			    		Connexion</button></a>';
		    	} 

			    if (!empty($_SESSION["user"])) {
			    	echo '<a href="/admin"><button type="button" class="btn btn-info navbar-btn navbar-right connecnav">Ajouter des photos</button></a>';
			    	echo '<p class="navbar-text navbar-right connecnav"> Connecté en tant que : <span id="pseudoConnec">
			    		'.$_SESSION["user"]["pseudo"].'</span></p>';
			    }
			?>
		</nav>

		<div class="container">
			<div class="main">
				<?= $this->yields('default'); ?>
			</div>
		</div> 

		
	</body>
</html>
