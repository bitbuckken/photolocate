<?php $villes = $this->tpl['vars']['villes']; ?>
<div class="center">
  <form enctype="multipart/form-data" class="form-horizontal" method="post" action="/traitement_photo">
    <fieldset>
      <legend>Ajout d'une photo</legend>
      <div class="form-group">
        <label class="col-md-4 control-label" for="latitude">Ville :</label>
        <div class="col-md-4">
          <select class="form-control input-md" name="ville" id="ville">
            <?php 
              foreach ($villes as $v) {
                echo '<option value="'.$v->ville_id.'">'.$v->ville_libelle.'</option>';
              }
            ?>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label" for="nomLieu">Nom du lieu :</label>
        <div class="col-md-4">
          <input class="form-control input-md" type="text" name="nomLieu" id="nomLieu" <?php if(!empty($this->tpl['vars']['lieu'])) { echo'value="'.$this->tpl['vars']['lieu'].'"';} ?> />
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label" for="latitude">Latitude :</label>
        <div class="col-md-4">
          <input class="form-control input-md" type="text" name="latitude" placeholder="6.174405" id="latitude" <?php if(!empty($this->tpl['vars']['latd'])) { echo'value="'.$this->tpl['vars']['latd'].'"';} ?> />
        </div>
      </div>
      <div class="form-group">
        <label for="longitude" class="col-md-4 control-label">Longitude :</label>
        <div class="col-md-4">
          <input class="form-control" type="text" name="longitude" id="longitude" placeholder="48.689983" <?php if(!empty($this->tpl['vars']['lng'])) { echo'value="'.$this->tpl['vars']['lng'].'"';} ?> />
        </div>
      </div>
      <div class="form-group">
        <label for="photo" class="col-md-4 control-label">Photo :</label>
        <div class="col-md-4">
          <input type="file" id="photo" value="upload" name="photo" />
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label" for="formInscription"></label>
        <div class="col-md-4">
          <button type="submit"  class="btn btn-primary">Ajouter une photo</button>
          <input type="hidden" name="csrf_token" value="<?php echo $_SESSION["csrf_token"]; ?>">
        </div>
      </div>
    </fieldset>
  </form>
  <?php 
  if(!empty($this->tpl['vars']['ErrorMess'])) {
    echo '<div class="alert alert-danger alert" role="alert">'.$this->tpl['vars']['ErrorMess'].'</div>';
  }
  if(!empty($this->tpl['vars']['result'])) {
    echo '<div class="alert alert-success alert" role="alert">'.$this->tpl['vars']['result'].'</div>';
  }
  if(!empty($this->tpl['vars']['ErreurPhoto'])) {
    echo '<div class="alert alert-danger alert" role="alert">'.$this->tpl['vars']['ErreurPhoto'].'</div>';
  }
  ?>
</div>