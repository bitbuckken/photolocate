<?php
session_cache_limiter(false);
session_start();
require_once '../vendor/autoload.php';// Autoload our dependencies with Composer
require_once '../config/config.php';// config APP

use MFWK\controllers\HomeController;
use MFWK\controllers\UserController;

$app = new \Slim\Slim();
$app->add(new \Slim\Middleware\ContentTypes());


$view = \MFWK\lib\View::getInstance($app);

$view->setLayout('index.tpl.php');

/*********** ROUTES ************/

$app->get('/', function () use ($view, $app) {
	$view->render('/accueil/contentAccueil.tpl.php');
})->name('home');

$app->get('/Play', function () use ($view, $app) {
	$view->setLayout('layouts/accueil.tpl.php');
	$view->render('angular/accueil.tpl.php');
})->name('Play');

$app->get('/connexion', function () use ($view, $app) {
	$view->render('/user/connexion.tpl.php');
})->name('connexionView');

$app->get('/deconnexion', function () use ($view, $app) {
	unset($_SESSION["user"]);
	session_destroy();
	$view->render('/accueil/contentAccueil.tpl.php');
})->name('deconnexion');

$app->post('/connexion', function () use ($view, $app) {
	$user = new UserController();
	$user->checkConnexion();
})->name('connexion');

$app->get('/inscription', function () use ($view, $app) {
	$view->render('/user/inscription.tpl.php');
})->name('inscriptionView');

$app->post('/inscription', function () use ($view, $app) {
	$user = new UserController();
	$user->inscriptionUser();
})->name('inscription');

$app->post('/jouer', function () use ($view, $app) {
	$control = new HomeController();
	$control->checkPseudo();
})->name('jouer');

$app->get('/admin', function () use ($view, $app) {
	$c = new \MFWK\controllers\PartieController;
	$c->ajoutPhotosAdmin();
})->name('admin');

$app->post('/traitement_photo', function () use ($view, $app) {
	$c = new \MFWK\controllers\PartieController;
	$c->traitementPhoto();
})->name('traitement_photo');


$app->get('/scores', function () use ($view, $app) {
		$c = new \MFWK\controllers\PartieController;
		$c->getScores();
		
})->name('scores');


$app->get('/angular/:filename', function ($filename) use ($app, $view){
	if(file_exists(APP_PATH.'/views/angular/'.$filename.'.tpl.php')){
		$view->setLayout('layouts/empty.tpl.php');
		$view->render('angular/'.$filename.'.tpl.php');//on force le .tpl.php pour un peu plus de sécu
	}
	else{
		die("nothing to display");
	}
});

/*********** ROUTES ************/



/* ------ API
* Utilisation des MiddleWares authenticate et toJson qui permettent respectivement
* de vérifier que l'utilisateur est connecté et de répondre en application/json
*/
$app->group('/play', function () use ($view, $app) {
	$c = new \MFWK\controllers\PartieController;
	$app->get('/scores/:nombre', function ($nombre) use ($view, $app, $c) { $c->getScores($nombre); });

	$app->get('/getPosition/:idPartie', function ($idPartie) use ($view, $app, $c) { $c->getPosition($idPartie); });
	
	$app->group('/games', function () use ($view, $app){



		$c = new \MFWK\controllers\PartieController;
		
		$app->get('/ville', function () use ($view, $app) { 
			$v = new \MFWK\controllers\VilleController;
			$v->getVille(); 
		});

		$app->post('/', function () use ($view, $app, $c) {
			$c->create_partie($app->request->post("pseudo"),$app->request->post("ville"));
		});

		$app->get('/:id/photos/', function ($id) use ($view, $app, $c) { $c->getPhoto($id,$app->request->get("token")); });
		
		$app->get('/:id', function ($id) use ($view, $app, $c) { $c->getParties($id, $app->request->get("token")); });

		$app->put('/:id/', function ($id) use ($view, $app, $c) { 

			//mise à jour de la partie, le score de l'utilisateur est enregistré, la partie est marquée comme achevée
			

			$c->finish($id, $app->request->put("s"));

		});

	});
});

$app->notFound(function () use ($app) {
	if($app->request->isXhr()){
		$app->contentType('application/json');
		$app->halt(404, json_encode(array('status' => array('statusCode' => 404, 'statusMessage' => 'Service inexistant'))));
	}
	else $app->redirect('404.html');
});

$app->run();
$view->compute();//ne fait rien si on est en XHR
