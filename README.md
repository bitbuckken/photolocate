# PhotoLocate

Le principe est du jeu est simple : 
    une photo est proposée au joueur, il doit indiquer la position de ce qu'elle représente sur une carte d'une ville.

## URL du site  : http://photolocate.lletang.com/


# Installation

## Fichier config/database.php

Créer le fichier `/config/database.php` avec vos informations de connexions.


```
#!php

<?php
// Database information
$settings = array(
	'driver' 	=> 'mysql',
	'host' 		=> '127.0.0.1',
	'database' 	=> 'photolocate',
	'username'	=> 'username',
	'password' 	=> 'password',
	'charset'	=> "utf8",
	'collation' => 'utf8_general_ci',
	'prefix' 	=> ''
);

// Bootstrap Eloquent ORM
$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory(new Illuminate\Container\Container);
$conn = $connFactory->make($settings);
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);
```


Le fichier n'est pas suivi par git, pour éviter de divulguer les identifiants de la DB.

Configurez les accès à la DB. La base de données doit exister avant de commencer.

## Virtual Host simple - SLIM

Pour accèder au site si test en local. 

De cette façon : http://photolocate, ou directement : photolocate 

Pensez à modifier le PATH du DocumentRoot et du Directory

	<VirtualHost *:80>
	    ServerName      photolocate
		DocumentRoot 	/data/www/photolocate/public
		ErrorDocument 	404 /404.html

		<Directory "/data/www/photolocate/public">
			AllowOverride All
			Order allow,deny
			Allow from all
		</Directory>
	</VirtualHost>

## Dossier public

Création des liens symboliques pour les fichiers javascripts et css dans le dossier public

	cd public
	mkdir assets
	cd assets
	ln -s ../../app/assets/stylesheets/dist/ stylesheets
	ln -s ../../app/assets/images images
	mkdir javascripts
	cd javascripts
	ln -s ../../../app/assets/javascripts/dist
	ln -s ../../../app/assets/javascripts/lib

## Compiler en continu les scripts coffee

	cd app/assets/javascripts/
	coffee -wo ./dist .

## Compiler en continu les styles

	cd app/assets/stylesheets/
	sass -w styles.scss:dist/styles.css